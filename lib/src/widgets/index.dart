/* Para centralizar el montón de widgets que van a haber */
export 'package:shoes_app/src/widgets/custom_appbar.dart';
export 'package:shoes_app/src/widgets/zapato_size.dart';
export 'package:shoes_app/src/widgets/zapato_desc.dart';
export 'package:shoes_app/src/widgets/AddToCart.dart';
export 'package:shoes_app/src/widgets/boton_naranja.dart';